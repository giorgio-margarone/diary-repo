import 'imports/agb_page_imports.dart';

class Agb extends StatefulWidget {
  const Agb({Key? key}) : super(key: key);

  @override
  _AgbState createState() => _AgbState();
}

class _AgbState extends State<Agb> {
  late Future<Album> futureAlbum;

  @override
  void initState() {
    super.initState();
    futureAlbum = fetchTermsOfUse();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBarWidget(
          title: AppLocalizations.of(context).agbAppBarTitle,
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.only(top: 15, right: 10, left: 10),
            child: FutureBuilder<Album>(
              future: futureAlbum,
              builder: (context, snapshot) {
                bool isGerman = Localizations.localeOf(context).languageCode == 'de';
                if (snapshot.hasData && !isGerman) {
                  return Html( data: snapshot.data!.content );
                } else if (snapshot.hasError || isGerman) {
                  return Html( data: getTermsOfUseGermanHtml() );
                }

                return const CircularProgressIndicator();
              },
            ),
          ),
        ),
      );
}
