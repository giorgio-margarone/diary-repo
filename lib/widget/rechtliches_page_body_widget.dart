import '/imports/rechtliches_page_body_widget_imports.dart';

class ImpressumButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          fixedSize: Size(250, 60),
          textStyle: TextStyle(fontSize: 20),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed('navToImpressum');
        },
        child: Text(
          AppLocalizations.of(context).legalImpressum,
        ),
      ),
    );
  }
}

class DatenschutzButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          fixedSize: Size(250, 60),
          textStyle: TextStyle(fontSize: 20),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed('navToDatenschutz');
        },
        child: Text(
          AppLocalizations.of(context).legalDatenschutz,
        ),
      ),
    );
  }
}

class AgbButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          fixedSize: Size(250, 60),
          textStyle: TextStyle(fontSize: 20),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed('navToAgb');
        },
        child: Text(
          AppLocalizations.of(context).legalAGB,
        ),
      ),
    );
  }
}

class LizenzenButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          fixedSize: Size(250, 60),
          textStyle: TextStyle(fontSize: 20),
        ),
        onPressed: () => showLicensePage(
            context: context,
            applicationName: '',
            applicationIcon: Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: Image.asset(
                'assets/licenses-logo.png',
                width: 120,
                height: 120,
              ),
            ),
            applicationVersion: '1.0.0',
            applicationLegalese: 'Copyright Georgios Margarone'),
        child: Text(
          AppLocalizations.of(context).legalLizenzen,
        ),
      ),
    );
  }
}
