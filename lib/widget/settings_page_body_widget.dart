import '/imports/settings_page_body_widget_imports.dart';

class AppearanceListTileWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(AppLocalizations.of(context).appearanceTitle),
      subtitle: Text(AppLocalizations.of(context).appearanceSubTitle),
      trailing: ChangeThemeButtonWidget(),
    );
  }
}
