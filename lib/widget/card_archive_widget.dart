import '/imports/card_archive_widget_imports.dart';
import 'package:intl/intl.dart';

class EntryCardWidget extends StatelessWidget {
  EntryCardWidget({
    Key? key,
    required this.entry,
    required this.index,
  }) : super(key: key);

  final EntryModel entry;
  final int index;

  @override
  Widget build(BuildContext context) {
    /// Pick colors from the accent colors based on index
    final color = Colors.grey.shade300;
    final time = DateFormat.yMMMd().format(entry.createdTime);
    final minHeight = getMinHeight(index);

    return Card(
      color: color,
      margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      child: Container(
        constraints: BoxConstraints(minHeight: minHeight),
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              time,
              style: TextStyle(color: Colors.grey.shade700),
            ),
            Divider(color: Colors.grey.shade600),
            Flexible(
              child: Container(
                child: Text(
                  entry.title,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Colors.grey.shade800,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(height: 7),
            Flexible(
              child: Container(
                child: Text(
                  entry.description,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: TextStyle(
                    color: Colors.grey.shade800,
                    fontSize: 14,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// To return different height for different widgets
  double getMinHeight(int index) {
    switch (index % 4) {
      case 0:
        return 100;
      case 1:
        return 150;
      case 2:
        return 150;
      case 3:
        return 100;
      default:
        return 100;
    }
  }
}
