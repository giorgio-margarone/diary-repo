import '/imports/entry_form_widget_imports.dart';
import 'package:flutter/services.dart';

class EntryFormWidget extends StatelessWidget {
  final bool? isImportant;
  final int? number;
  final String? title;
  final String? description;
  final ValueChanged<bool> onChangedImportant;
  final ValueChanged<int> onChangedNumber;
  final ValueChanged<String> onChangedTitle;
  final ValueChanged<String> onChangedDescription;

  const EntryFormWidget({
    Key? key,
    this.isImportant = false,
    this.number = 0,
    this.title = '',
    this.description = '',
    required this.onChangedImportant,
    required this.onChangedNumber,
    required this.onChangedTitle,
    required this.onChangedDescription,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              buildTitle(context),
              SizedBox(height: 30),
              buildDescription(context),
            ],
          ),
        ),
      );

  Widget buildTitle(BuildContext context) => TextFormField(
        inputFormatters: [
          LengthLimitingTextInputFormatter(40),
        ],
        maxLength: 40,
        maxLines: 1,
        initialValue: title,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          border: InputBorder.none,
          hintText: AppLocalizations.of(context).entryTitlePlaceholder,
        ),
        validator: (title) => title != null && title.isEmpty
            ? AppLocalizations.of(context).entryTitleValidator
            : null,
        onChanged: onChangedTitle,
      );

  Widget buildDescription(BuildContext context) => TextFormField(
        inputFormatters: [
          LengthLimitingTextInputFormatter(400),
        ],
        maxLength: 400,
        minLines: 1,
        maxLines: 20,
        initialValue: description,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          border: InputBorder.none,
          hintText: AppLocalizations.of(context).entryTextFieldPlaceholder,
        ),
        validator: (title) => title != null && title.isEmpty
            ? AppLocalizations.of(context).entryTextFieldValidator
            : null,
        onChanged: onChangedDescription,
      );
}
