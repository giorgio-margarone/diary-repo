import '/imports/theme_switch_widget_imports.dart';

class ChangeThemeButtonWidget extends StatelessWidget {
  @override

  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context);

    return Switch.adaptive(
      inactiveTrackColor: Colors.grey.shade700,
      activeColor: Colors.grey.shade500,
      value: themeProvider.isDarkMode,
      onChanged: (value) {
        final provider = Provider.of<ThemeProvider>(context, listen: false);
        provider.toggleTheme(value);
      },
    );
  }
}