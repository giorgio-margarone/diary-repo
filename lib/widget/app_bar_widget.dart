import 'package:flutter/material.dart';

// Not used for AppBar in Home Page, in Entry Edit Page, Entry Page 1 & 2, 
class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  const AppBarWidget({
    required this.title,
  });

  final String title;

  @override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios_new),
        onPressed: () => Navigator.of(context).pop(),
      ),
      title: Text(title),
    );
  }
}

class LogoAppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: SizedBox(
        height: kToolbarHeight,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 5),
          child: Image.asset(
            'assets/licenses-logo.png',
          ),
        ),
      ),
    );
  }
}
