import '/imports/homepage_body_widget_imports.dart';

class EntryButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          fixedSize: Size(200, 60),
          textStyle: TextStyle(fontSize: 20),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed('navToEditEntry2');
        },
        child: Text(
          AppLocalizations.of(context).entry,
        ),
      ),
    );
  }
}

class DiaryButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          fixedSize: Size(200, 60),
          textStyle: TextStyle(fontSize: 20),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed('navToArchive');
        },
        child: Text(
          AppLocalizations.of(context).archive,
        ),
      ),
    );
  }
}

class SettingsButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          fixedSize: Size(200, 60),
          textStyle: TextStyle(fontSize: 20),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed('navToSettings');
        },
        child: Text(
          AppLocalizations.of(context).settings,
        ),
      ),
    );
  }
}

class FooterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      elevation: 0.0,
      onTap: (value) {
        if (value == 1) Navigator.of(context).pushNamed('navToRechtliches');
      },
      items: [
        BottomNavigationBarItem(
          icon: Text(''),
          label: '',
        ),
        BottomNavigationBarItem(
          icon: Text(
            AppLocalizations.of(context).legal,
            textAlign: TextAlign.center,
          ),
          label: '',
        ),
        BottomNavigationBarItem(
          icon: Text(''),
          label: '',
        ),
      ],
    );
  }
}
