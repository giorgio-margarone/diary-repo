import 'package:flutter/material.dart';
import 'package:url_launcher/link.dart';

class LinkWidget extends StatefulWidget {
  final String url;
  final String linkName;

  const LinkWidget({
    Key? key,
    required this.url,
    required this.linkName,
  }) : super(key: key);

  @override
  State<LinkWidget> createState() => _LinkWidgetState();
}

class _LinkWidgetState extends State<LinkWidget> {
  @override
  Widget build(BuildContext context) {
    return Link(
      target: LinkTarget.self,
      uri: Uri.parse(widget.url),
      builder: (context, followLink) => TextButton(
        child: Text(
          widget.linkName,
          style: TextStyle(
            color: Colors.blue,
            decoration: TextDecoration.underline,
          ),
        ),
        onPressed: followLink,
      ),
    );
  }
}
