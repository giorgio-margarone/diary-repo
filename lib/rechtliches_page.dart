import 'imports/rechtliches_page_imports.dart';
import 'widget/rechtliches_page_body_widget.dart';

class Rechtliches extends StatefulWidget {
  @override
  _RechtlichesState createState() => _RechtlichesState();
}

class _RechtlichesState extends State<Rechtliches> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBarWidget(
          title: AppLocalizations.of(context).legalAppBarTitle,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ImpressumButtonWidget(),
            SizedBox(height: 30),
            DatenschutzButtonWidget(),
            SizedBox(height: 30),
            AgbButtonWidget(),
            SizedBox(height: 30),
            LizenzenButtonWidget(),
          ],
        ),
      );
}
