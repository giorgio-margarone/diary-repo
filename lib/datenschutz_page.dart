import 'imports/datenschutz_page_imports.dart';

class Datenschutz extends StatefulWidget {
  const Datenschutz({Key? key}) : super(key: key);

  @override
  _DatenschutzState createState() => _DatenschutzState();
}

class _DatenschutzState extends State<Datenschutz> {
  late Future<Album> futureAlbum;

  @override
  void initState() {
    super.initState();
    futureAlbum = fetchPrivatePolicy();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBarWidget(
          title: AppLocalizations.of(context).datenschutzAppBarTitle,
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.only(top: 15, right: 10, left: 10),
            child: FutureBuilder<Album>(
              future: futureAlbum,
              builder: (context, snapshot) {
                bool isGerman = Localizations.localeOf(context).languageCode == 'de';
                if (snapshot.hasData && !isGerman) {
                  return Html( data: snapshot.data!.content );
                } else if (snapshot.hasError || isGerman) {
                  return Html( data: getPrivatePolicyGermanHtml() );
                }

                return const CircularProgressIndicator();
              },
            ),
          ),
        ),
      );
}
