import 'imports/main_imports.dart';
import 'package:provider/provider.dart';
import 'validation/terms_and_policy_validation.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final String title = 'Diary';

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
    create: (context) => ThemeProvider(),
      builder: (context, _) {
        final themeProvider = Provider.of<ThemeProvider>(context);

        return MaterialApp(
          debugShowCheckedModeBanner: false,
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: AppLocalizations.supportedLocales,
          title: title,
          themeMode: themeProvider.themeMode,
          theme: MyThemes.lightTheme,
          darkTheme: MyThemes.darkTheme,
          home: Splash(),
          routes: {
            'navToHome': (_) => Homepage(),
            'navToEditEntry2': (_) => EditEntryPage2(),
            'navToEditEntry': (_) => EditEntryPage(),
            'navToArchive': (_) => Archive(),
            'navToSettings': (_) => Settings(),
            'navToRechtliches': (_) => Rechtliches(),
            'navToImpressum': (_) => Impressum(),
            'navToDatenschutz': (_) => Datenschutz(),
            'navToAgb': (_) => Agb(),
          },
        );
      },
  );
}