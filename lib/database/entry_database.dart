import '/imports/entry_database_imports.dart';

class EntryDatabase {
  static final EntryDatabase instance = EntryDatabase._init();

  static Database? _database;

  EntryDatabase._init();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('entry.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    final idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    final textType = 'TEXT NOT NULL';
    final boolType = 'BOOLEAN NOT NULL';
    final integerType = 'INTEGER NOT NULL';

    await db.execute('''
      CREATE TABLE $tableEntry (
        ${EntryFields.id} $idType,
        ${EntryFields.isImportant} $boolType,
        ${EntryFields.number} $integerType,
        ${EntryFields.title} $textType,
        ${EntryFields.description} $textType,
        ${EntryFields.time} $textType
      )
    ''');
  }

  Future<EntryModel> create(EntryModel entryModel) async {
    final db = await instance.database;

    final id = await db.insert(tableEntry, entryModel.toJson());

    return entryModel.copy(id: id);
  }

  Future<EntryModel> readEntryModel(int id) async {
    final db = await instance.database;

    final maps = await db.query(
      tableEntry,
      columns: EntryFields.values,
      where: '${EntryFields.id} = ?',
      whereArgs: [id],
    );

    if (maps.isNotEmpty) {
      return EntryModel.fromJson(maps.first);
    } else {
      throw Exception('ID $id not found');
    }
  }

  Future<List<EntryModel>> readAllEntries() async {
    final db = await instance.database;

    final orderBy = '${EntryFields.time} DESC';

    final result = await db.query(tableEntry, orderBy: orderBy);

    return result.map((json) => EntryModel.fromJson(json)).toList();
  }

  Future<int> update(EntryModel entryModel) async {
    final db = await instance.database;

    return db.update(
      tableEntry,
      entryModel.toJson(),
      where: '${EntryFields.id} = ?',
      whereArgs: [entryModel.id],
    );
  }

  Future<int> delete(int id) async {
    final db = await instance.database;

    return await db.delete(
      tableEntry,
      where: '${EntryFields.id} = ?',
      whereArgs: [id],
    );
  }

  Future close() async {
    final db = await instance.database;
    _database = null;

    db.close();
  }
}