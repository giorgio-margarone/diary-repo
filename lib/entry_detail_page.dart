import '/imports/entry_detail_page_imports.dart';
import 'package:intl/intl.dart';

class EntryDetailPage extends StatefulWidget {
  final int entryId;

  const EntryDetailPage({
    Key? key,
    required this.entryId,
  }) : super(key: key);

  @override
  _EntryDetailPageState createState() => _EntryDetailPageState();
}

class _EntryDetailPageState extends State<EntryDetailPage> {
  late EntryModel entry;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();

    refreshEntry();
  }

  Future refreshEntry() async {
    setState(() => isLoading = true);

    this.entry = await EntryDatabase.instance.readEntryModel(widget.entryId);

    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_new),
            onPressed: () => Navigator.of(context).pop(),
          ),
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context).entryEditAppBarTitle,
          ),
          actions: [
            editButton(),
            deleteButton(),
          ],
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Padding(
                padding: EdgeInsets.all(20),
                child: ListView(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  children: [
                    Text(
                      DateFormat.yMMMd().format(entry.createdTime),
                    ),
                    Divider(color: Colors.grey.shade600),
                    SizedBox(height: 30),
                    Text(
                      entry.title,
                      style: TextStyle(
                        fontSize: 21,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 25),
                    Text(
                      entry.description,
                      style: TextStyle(fontSize: 18),
                    ),
                  ],
                ),
              ),
      );

  Widget editButton() => IconButton(
      icon: Icon(Icons.edit_outlined),
      onPressed: () async {
        if (isLoading) return;

        await Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => EditEntryPage(entry: entry),
        ));

        refreshEntry();
      });

  Widget deleteButton() => IconButton(
    icon: Icon(Icons.delete),
    onPressed: () async {
      showAlertDialog(context);
    },
  );

  showAlertDialog(BuildContext context) {
    void _showSnackbarDeleted() {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        elevation: 2,
        duration: Duration(seconds: 4),
        content: Text(
          AppLocalizations.of(context).snackbarDeleted,
        ),
        backgroundColor: Colors.red,
      ));
    }

    Widget cancelButton = TextButton(
      child: Text(AppLocalizations.of(context).alertCancel),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget deleteButton = TextButton(
      child: Text(AppLocalizations.of(context).alertDelete),
      onPressed: () async {
        await EntryDatabase.instance.delete(widget.entryId);

        _showSnackbarDeleted();

        Navigator.of(context).pushNamedAndRemoveUntil(
            'navToHome', (Route<dynamic> route) => false);

        Navigator.of(context).pushNamedAndRemoveUntil(
            'navToArchive', ModalRoute.withName('navToHome'));
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text(AppLocalizations.of(context).alertTitle),
      content: Text(AppLocalizations.of(context).alertSubTitle),
      actions: [
        cancelButton,
        deleteButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
