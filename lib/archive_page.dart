import 'imports/archive_page_imports.dart';

class Archive extends StatefulWidget {
  @override
  _ArchiveState createState() => _ArchiveState();
}

class _ArchiveState extends State<Archive> {
  late List<EntryModel> entries;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();

    refreshEntries();
  }

  @override
  void dispose() {
    EntryDatabase.instance.close();

    super.dispose();
  }

  Future refreshEntries() async {
    setState(() => isLoading = true);

    this.entries = await EntryDatabase.instance.readAllEntries();

    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBarWidget(
          title: AppLocalizations.of(context).archiveAppBarTitle,
        ),
        body: Center(
          child: isLoading
              ? CircularProgressIndicator()
              : entries.isEmpty
                  ? Text(
                      AppLocalizations.of(context).noEntries,
                      style: TextStyle(fontSize: 18),
                    )
                  : buildEntries(),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
          ),
          onPressed: () async {
            Navigator.of(context).pushNamed('navToEditEntry');

            refreshEntries();
          },
        ),
      );

  Widget buildEntries() => GridView.builder(
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: MediaQuery.of(context).size.width * 0.5,
          childAspectRatio: 3 / 2,
          mainAxisSpacing: 10,
        ),
        itemCount: entries.length,
        itemBuilder: (context, index) {
          final entry = entries[index];

          return GestureDetector(
            onTap: () async {
              await Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => EntryDetailPage(entryId: entry.id!),
              ));

              refreshEntries();
            },
            child: EntryCardWidget(entry: entry, index: index),
          );
        },
      );
}
