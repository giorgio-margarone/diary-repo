import 'imports/impressum_page_imports.dart';

class Impressum extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBarWidget(
          title: AppLocalizations.of(context).impressumAppBarTitle,
        ),
        body: Container(
          alignment: Alignment.center,
          child: Text(
            AppLocalizations.of(context).impressumText,
            textAlign: TextAlign.center,
          ),
        ),
      );
}
