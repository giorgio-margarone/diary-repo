import '/imports/entry_edit_page_1_imports.dart';

// Used when navigated from Archive or Detail Page

class EditEntryPage extends StatefulWidget {
  final EntryModel? entry;

  const EditEntryPage({
    Key? key,
    this.entry,
  }) : super(key: key);

  @override
  _EditEntryPageState createState() => _EditEntryPageState();
}

class _EditEntryPageState extends State<EditEntryPage> {
  final _formKey = GlobalKey<FormState>();
  late bool isImportant;
  late int number;
  late String title;
  late String description;

  @override
  void initState() {
    super.initState();

    isImportant = widget.entry?.isImportant ?? false;
    number = widget.entry?.number ?? 0;
    title = widget.entry?.title ?? '';
    description = widget.entry?.description ?? '';
  }

  void _showSnackbarSuccess() {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      elevation: 2,
      duration: Duration(seconds: 4),
      content: Text(
        AppLocalizations.of(context).snackbarSaved,
      ),
      backgroundColor: Colors.green.shade600,
    ));
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_new),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text(AppLocalizations.of(context).entryAppBarTitle),
          actions: [
            buildButton(),
          ],
        ),
        body: Form(
          key: _formKey,
          child: EntryFormWidget(
            isImportant: isImportant,
            number: number,
            title: title,
            description: description,
            onChangedImportant: (isImportant) =>
                setState(() => this.isImportant = isImportant),
            onChangedNumber: (number) => setState(() => this.number = number),
            onChangedTitle: (title) => setState(() => this.title = title),
            onChangedDescription: (description) =>
                setState(() => this.description = description),
          ),
        ),
      );

  Widget buildButton() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          primary: Colors.white,
          side: BorderSide(
            color: Colors.white,
          ),
        ),
        onPressed: addOrUpdateEntry,
        child: Text(AppLocalizations.of(context).save),
      ),
    );
  }

  void addOrUpdateEntry() async {
    final isValid = _formKey.currentState!.validate();

    if (isValid) {
      final isUpdating = widget.entry != null;

      if (isUpdating) {
        await updateEntry();
      } else {
        await addEntry();
      }

      _showSnackbarSuccess();

      Navigator.of(context).pushNamedAndRemoveUntil(
          'navToHome', (Route<dynamic> route) => false);

      Navigator.of(context).pushNamedAndRemoveUntil(
          'navToArchive', ModalRoute.withName('navToHome'));
    }
  }

  Future updateEntry() async {
    final entry = widget.entry!.copy(
      isImportant: isImportant,
      number: number,
      title: title,
      description: description,
    );

    await EntryDatabase.instance.update(entry);
  }

  Future addEntry() async {
    final entry = EntryModel(
      title: title,
      isImportant: true,
      number: number,
      description: description,
      createdTime: DateTime.now(),
    );

    await EntryDatabase.instance.create(entry);
  }
}
