import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

// only the English version is fetched
// German version is manually added from folder html/

Future<Album> fetchTermsOfUse() async {
  final response = await http
      .get(Uri.parse('https://www.iubenda.com/api/terms-and-conditions/50292367/no-markup'));

  if (response.statusCode == 200) {
    return Album.fromJson(jsonDecode(response.body));
  } else {
    throw Exception('Failed to fetch Terms of Use');
  }
}

class Album {
  final String content;

  Album({
    required this.content,
  });

  factory Album.fromJson(Map<String, dynamic> json) {
    return Album(
      content: json['content'],
    );
  }
}