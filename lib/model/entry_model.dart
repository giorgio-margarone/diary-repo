final String tableEntry = 'entries';

class EntryFields {
  static final List<String> values = [
    /// Add all fields
    id, isImportant, number, title, description, time
  ];

  static final String id = '_id';
  static final String isImportant = 'isImportant';
  static final String number = 'number';
  static final String title = 'title';
  static final String description = 'description';
  static final String time = 'time';
}

class EntryModel {
  final int? id;
  final bool isImportant;
  final int number;
  final String title;
  final String description;
  final DateTime createdTime;

  const EntryModel({
    this.id,
    required this.isImportant,
    required this.number,
    required this.title,
    required this.description,
    required this.createdTime,
  });

  EntryModel copy({
    int? id,
    bool? isImportant,
    int? number,
    String? title,
    String? description,
    DateTime? createdTime,
  }) =>
  EntryModel(
    id: id ?? this.id,
    isImportant: isImportant ?? this.isImportant,
    number: number ?? this.number,
    title: title ?? this.title,
    description: description ?? this.description,
    createdTime: createdTime ?? this.createdTime,
  );

  static EntryModel fromJson(Map<String, Object?> json) => EntryModel(
    id: json[EntryFields.id] as int?,
    isImportant: json[EntryFields.isImportant] == 1,
    number: json[EntryFields.number] as int,
    title: json[EntryFields.title] as String,
    description: json[EntryFields.description] as String,
    createdTime: DateTime.parse(json[EntryFields.time] as String),
  );

  Map<String, Object?> toJson() => {
    EntryFields.id: id,
    EntryFields.title: title,
    EntryFields.isImportant: isImportant ? 1 : 0,
    EntryFields.number: number,
    EntryFields.description: description,
    EntryFields.time: createdTime.toIso8601String(),
  };
}