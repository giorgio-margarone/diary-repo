import '/imports/theme_provider_imports.dart';
import 'package:flutter/cupertino.dart';

class ThemeProvider extends ChangeNotifier {
  ThemeMode themeMode = ThemeMode.light;

  bool get isDarkMode => themeMode == ThemeMode.dark;

  void toggleTheme(bool isOn) {
    themeMode = isOn ? ThemeMode.dark : ThemeMode.light;
    notifyListeners();
  }
}

class MyThemes {
  static final darkTheme = ThemeData(
    fontFamily: GoogleFonts.poppins().fontFamily,
    scaffoldBackgroundColor: Colors.grey.shade800,
    colorScheme: ColorScheme.dark(),
    cupertinoOverrideTheme: CupertinoThemeData(
      // Affect only on iOS - when primaryColor is changed, change selectionHandleColor too.
      primaryColor: Colors.teal.shade200,
    ),
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: Colors.white,
      selectionColor: Colors.grey.shade900,
      // Affect only on Android - when selectionHandleColor is changed, change cupertinoOverrideTheme too.
      selectionHandleColor: Colors.teal.shade200,
    ),
    appBarTheme: AppBarTheme(
      color: Colors.grey.shade900,
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: Colors.grey.shade800,
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        primary: Colors.white,
      ),
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Colors.cyan.shade800,
      foregroundColor: Colors.white,
    ),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        primary: Colors.white,
      ),
    ),
    dialogTheme: DialogTheme(
      contentTextStyle: TextStyle(
        color: Colors.white60,
      ),
    ),
    snackBarTheme: SnackBarThemeData(
      behavior: SnackBarBehavior.floating,
      contentTextStyle: TextStyle(
        color: Colors.white,
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      errorStyle: TextStyle(color: Colors.red.shade400),
      errorBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red.shade400),
      ),
      focusedErrorBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red.shade400),
      ),
    ),
  );

  static final lightTheme = ThemeData(
    fontFamily: GoogleFonts.poppins().fontFamily,
    scaffoldBackgroundColor: Colors.white,
    colorScheme: ColorScheme.light(),
    cupertinoOverrideTheme: CupertinoThemeData(
      // Affect only on iOS - when primaryColor is changed, change selectionHandleColor too.
      primaryColor: Colors.blueGrey.shade300,
    ),
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: Colors.black,
      selectionColor: Colors.blueGrey.shade100,
      // Affect only on Android - when selectionHandleColor is changed, change cupertinoOverrideTheme too.
      selectionHandleColor: Colors.blueGrey.shade300,
    ),
    appBarTheme: AppBarTheme(
      color: Colors.blueGrey,
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: Colors.white,
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        primary: Colors.black,
      ),
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Colors.blueGrey,
      foregroundColor: Colors.white,
    ),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(
        primary: Colors.black,
      ),
    ),
    dialogTheme: DialogTheme(
      contentTextStyle: TextStyle(
        color: Colors.black87,
      ),
    ),
    snackBarTheme: SnackBarThemeData(
      behavior: SnackBarBehavior.floating,
    ),
    inputDecorationTheme: InputDecorationTheme(
      errorStyle: TextStyle(color: Colors.red),
      errorBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
      focusedErrorBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
    ),
  );
}
