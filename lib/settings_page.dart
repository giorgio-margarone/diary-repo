import 'imports/settings_imports.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: AppLocalizations.of(context).settingsAppBarTitle,
      ),
      body: ListView(
        children: <Widget>[
          AppearanceListTileWidget(),
        ],
      ),
    );
  }
}
