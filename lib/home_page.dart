import 'imports/home_page_imports.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: LogoAppBarWidget(),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            EntryButtonWidget(),
            SizedBox(height: 30),
            DiaryButtonWidget(),
            SizedBox(height: 30),
            SettingsButtonWidget(),
          ],
        ),
        bottomNavigationBar: FooterWidget(),
      );
}
