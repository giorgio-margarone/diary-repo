import 'package:flutter/material.dart';
import '/widget/link_widget.dart';
import '/widget/app_bar_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:after_layout/after_layout.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '/home_page.dart';

class ValidationTermsAndPolicy extends StatefulWidget {
  @override
  _ValidationTermsAndPolicyState createState() =>
      _ValidationTermsAndPolicyState();
}

class _ValidationTermsAndPolicyState extends State<ValidationTermsAndPolicy> {
  String linkPrivatePolicy = 'https://www.iubenda.com/privacy-policy/50292367';
  String linkTermOfUse = 'https://www.iubenda.com/terms-and-conditions/50292367';

  bool checkBoxValue = false;

  goToHomepage() {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => Homepage()),
    );
  }

  setLegalValidationTrue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('accepted', true);
  }

  validationSubmitted() {
    goToHomepage();
    setLegalValidationTrue();
  }

  @override
  Widget build(BuildContext context) {
    bool isGerman = Localizations.localeOf(context).languageCode == 'de';

    return Scaffold(
      appBar: LogoAppBarWidget(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: !isGerman
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      LinkWidget(
                        url: linkPrivatePolicy,
                        linkName: 'Private Policy',
                      ),
                      LinkWidget(
                        url: linkTermOfUse,
                        linkName: 'Terms of Use',
                      )
                    ],
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed('navToAgb');
                        },
                        child: Text(
                          AppLocalizations.of(context).legalAGB,
                          style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed('navToDatenschutz');
                        },
                        child: Text(
                          AppLocalizations.of(context).legalDatenschutz,
                          style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      )
                    ],
                  ),
          ),
          SizedBox(height: 30),
          Row(
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.25,
                child: Checkbox(
                  activeColor: Colors.blueGrey,
                  side: BorderSide(color: Colors.red, width: 2),
                  value: checkBoxValue,
                  onChanged: (value) {
                    setState(() {
                      checkBoxValue = value ?? false;
                    });
                  },
                ),
              ),
              Flexible(
                child: Padding(
                  padding: EdgeInsets.only(left: 0, right: 40),
                  child: Text(
                    AppLocalizations.of(context).validationTermsAcceptedText,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 30),
          ElevatedButton(
            onPressed: !checkBoxValue ? null : () => validationSubmitted(),
            child: Text(
              AppLocalizations.of(context).validationSubmitButton,
            ),
            style: ElevatedButton.styleFrom(
              primary: Colors.blueGrey,
            ),
          ),
        ],
      ),
    );
  }
}

class Splash extends StatefulWidget {
  @override
  SplashState createState() => SplashState();
}

class SplashState extends State<Splash> with AfterLayoutMixin<Splash> {
  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool agreementState = (prefs.getBool('accepted') ?? false);

    if (agreementState) {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => Homepage()),
      );
    } else {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => ValidationTermsAndPolicy()),
      );
    }
  }

  @override
  void afterFirstLayout(BuildContext context) => checkFirstSeen();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Text('Loading...'),
      ),
    );
  }
}
